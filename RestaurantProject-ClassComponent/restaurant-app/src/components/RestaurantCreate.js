import React from 'react'
import NavbarMenu from './NavbarMenu';

export default class RestaurantCreate extends React.Component{

    constructor(){
        super();
        this.state={
            name:"",
            address:"",
            email:"",
            rating:""
        }
    }
    submit()
    {
        fetch('http://localhost:3000/restaurant',{
            method:"POST",
            headers:{
                'Content-Type':'application/json',
                'Accept':'application/json'
            },
            body:JSON.stringify(this.state)
        }).then((resp)=>{
            resp.json().then((result)=>{
                console.warn("Result", result)
                alert('Restaurant has been added')
            })
        })
    }
    render(){

        return(
            <div>
                <NavbarMenu/>
                <h2>Restaurant Create</h2>
                <input type="text" placeholder='Enter Restaurant Name' onChange={(e)=>this.setState({name:e.target.value})}/><br/><br/>
                <input type="text" placeholder='Enter Address' onChange={(e)=>this.setState({address:e.target.value})}/><br/><br/>
                <input type="text" placeholder='Enter Email' onChange={(e)=>this.setState({email:e.target.value})}/><br/><br/>
                <input type="text" placeholder='Enter Rating' onChange={(e)=>this.setState({rating:e.target.value})}/><br/><br/>
                <button onClick={()=>this.submit()}>Add Restaurant</button>
            </div>
        )
    }
}