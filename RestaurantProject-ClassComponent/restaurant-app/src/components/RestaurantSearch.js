import React, { Component } from 'react';
import {Table, Form, Container} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faEdit, faTrash} from '@fortawesome/free-solid-svg-icons'
import NavbarMenu from './NavbarMenu';

class RestaurantSearch extends Component {

    constructor() {
        super()
        this.state = {
            searchData: "",
            noData:false,
            lastSearch:""
        }
    }
    search(key) {
        this.setState({lastSearch:key})
        console.warn(key)
        fetch('http://localhost:3000/restaurant?q=' + key).then((resp) => {
            resp.json().then((result) => {
                console.warn(result)
                if(result.length>0)
                {
                    this.setState({ searchData: result, noData:false})
                }
                else{
                    this.setState({noData:true, searchData:null})
                }
                
            })
        })
    }
    delete(id)
    {
        fetch('http://localhost:3000/restaurant/'+id,{
            method:"DELETE"
        }).then((resp)=>{
            resp.json().then((result)=>{
                console.warn(result)
                alert('Restaurant Deleted Successfullly')
                this.search(this.state.lastSearch)
            })
        })
    }
    render() {
        return (
            <Container>
                <NavbarMenu/>
                <h2>Restaurant Search</h2>
                <Form.Control type="text" placeholder="Search Restaurant" onChange={(e) => this.search(e.target.value)}/>
                {
                    this.state.searchData ?
                        <Table striped bordered hover>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Restaurant Name</th>
                                    <th>Address</th>
                                    <th>Eamil</th>
                                    <th>Rating</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.searchData.map((item) =>
                                        <tr>
                                            <td>{item.id}</td>
                                            <td>{item.name}</td>
                                            <td>{item.address}</td>
                                            <td>{item.email}</td>
                                            <td>{item.rating}</td>
                                            <td>
                                                <Link to={"/update/" + item.id}><FontAwesomeIcon icon={faEdit} /></Link>
                                                <span onClick={() => this.delete(item.id)}><FontAwesomeIcon icon={faTrash} color="red" /></span>
                                            </td>
                                        </tr>
                                    )
                                }
                            </tbody>
                        </Table>
                        :
                        null
                }
                {
                    this.state.noData ?
                    <h3>No Data Found</h3>
                    :
                    null
                }
            </Container>
        );
    }
}

export default RestaurantSearch;