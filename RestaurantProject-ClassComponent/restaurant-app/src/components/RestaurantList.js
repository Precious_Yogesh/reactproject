import React from 'react';
import { Container, Table } from 'react-bootstrap';
import {Link} from 'react-router-dom';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faEdit, faTrash} from '@fortawesome/free-solid-svg-icons'
import NavbarMenu from './NavbarMenu';

export default class RestaurantList extends React.Component {

    constructor() {
        super();
        this.state = {
            data: ""
        }
    }
    getApidata()
    {
        fetch('http://localhost:3000/restaurant').then((resp) => {
            resp.json().then((result) => {
                console.warn()
                this.setState({
                     data: result
                })
            })
        })
    }
    componentDidMount() {
        this.getApidata();
    }
    delete(id)
    {
        fetch('http://localhost:3000/restaurant/'+id,{
            method:"DELETE"
        }).then((resp)=>{
            resp.json().then((result)=>{
                console.warn(result)
                alert('Restaurant Deleted Successfullly')
                this.getApidata();
            })
        })
    }
    render() {
        return (
            <Container>
                <NavbarMenu/>
                <h2>Restaurant List</h2>
                {
                    this.state.data ?
                        <Table striped bordered hover>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Restaurant Name</th>
                                    <th>Address</th>
                                    <th>Eamil</th>
                                    <th>Rating</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.data.map((item) =>
                                        <tr>
                                            <td>{item.id}</td>
                                            <td>{item.name}</td>
                                            <td>{item.address}</td>
                                            <td>{item.email}</td>
                                            <td>{item.rating}</td>
                                            <td>
                                                <Link to={"/update/"+item.id}><FontAwesomeIcon icon={faEdit}/></Link>
                                                <span onClick={()=>this.delete(item.id)}><FontAwesomeIcon icon={faTrash} color="red"/></span>
                                                </td>
                                        </tr>
                                    )
                                }
                            </tbody>
                        </Table>
                        :
                        null
                }
            </Container>
        )
    }
}