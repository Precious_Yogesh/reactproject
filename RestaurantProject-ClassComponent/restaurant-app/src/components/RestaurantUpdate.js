import React, { Component } from 'react';
import NavbarMenu from './NavbarMenu';

class RestaurantUpdate extends Component {

    constructor(){
        super();
        this.state={
            name:"",
            address:"",
            email:"",
            rating:"",
            id:""
        }
    }
    componentDidMount()
    {
        fetch('http://localhost:3000/restaurant/'+this.props.match.params.id).then((resp)=>{
            resp.json().then((result)=>{
                console.warn("Update", result)
                this.setState({
                    name:result.name,
                    address:result.address,
                    email:result.email,
                    rating:result.rating,
                    id:result.id
                })
            })
        })
    }
    update(){
        fetch('http://localhost:3000/restaurant/'+this.state.id,{
            method:"PUT",
            headers:{
                'Content-Type':'application/json',
                'Accept':'application/json'
            },
            body:JSON.stringify(this.state)
        }).then((resp)=>{
            resp.json().then((result)=>{
                console.warn(result)
                alert('Restaurant has been updated')
            })
        })
    }
    render() {
        return (
            <div>
                <NavbarMenu/>
                <h2>Restaurant Update</h2>
                <input type="text" placeholder='Restaurant Name' onChange={(e)=>this.setState({name:e.target.value})} value={this.state.name}/><br/><br/>
                <input type="text" placeholder='Address' onChange={(e)=>this.setState({address:e.target.value})} value={this.state.address}/><br/><br/>
                <input type="text" placeholder='Email' onChange={(e)=>this.setState({email:e.target.value})} value={this.state.email}/><br/><br/>
                <input type="text" placeholder='Rating' onChange={(e)=>this.setState({rating:e.target.value})} value={this.state.rating}/><br/><br/>
                <button onClick={()=>this.update()}>Update Restaurant</button>
            </div>
        );
    }
}

export default RestaurantUpdate;