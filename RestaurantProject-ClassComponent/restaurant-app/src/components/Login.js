import React from 'react'
import NavbarMenu from './NavbarMenu'

export default class Login extends React.Component{

    constructor(){
        super()
        this.state={
            name:"",
            pass:""
        }
    }
    login()
    {
        console.warn(this.state)
        fetch('http://localhost:3000/login?q='+this.state.name).then((resp)=>{
            resp.json().then((result)=>{
                console.warn("Result", result)
                if(result.length>0)
                {
                    console.warn(this.props)
                    localStorage.setItem('login', JSON.stringify(result))
                    console.warn(this.props.history.push('list'))
                }
                else{
                    alert('Please Check Username and Password')
                }            
            })
        })
    }
    render(){
        return(
            <div>
                <NavbarMenu/>
                <h2>Login Page</h2>
                <input type="text" placeholder='Enter Username' onChange={(e)=>this.setState({name:e.target.value})}/><br/><br/>
                <input type="Password" placeholder='Enter Password' onChange={(e)=>this.setState({pass:e.target.value})}/><br/><br/>
                <button onClick={()=>this.login()}>Login</button>
            </div>
        )
    }
}