import React from 'react';
import NavbarMenu from './NavbarMenu';

class RestaurantDetail extends React.Component {
    render() {
        return (
            <div>
                <NavbarMenu/>
                <h2>Restaurant Details</h2>
            </div>
        );
    }
}

export default RestaurantDetail;