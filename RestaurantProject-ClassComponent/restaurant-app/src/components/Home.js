import React from 'react'
import NavbarMenu from './NavbarMenu';

class Home extends React.Component{
    render(){
        return(
            <div>
                <NavbarMenu/>
                <h2>Welcome To Home</h2>
            </div>
        )
    }
}
export default Home;