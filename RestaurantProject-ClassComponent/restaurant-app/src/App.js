import logo from './logo.svg';
import './App.css';
import Login from './components/Login';
import Home from './components/Home';
import RestaurantList from './components/RestaurantList';
import RestaurantDetail from './components/RestaurantDetail';
import RestaurantSearch from './components/RestaurantSearch';
import RestaurantCreate from './components/RestaurantCreate';
import RestaurantUpdate from './components/RestaurantUpdate';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Logout from './components/Logout';
import Protected from './components/Protected';

function App() {
  return (
    <div className="App">
      <Router>
        <Route exact path="/login"
        render={props=>(
          <Login {...props}/>
        )}
        >
        </Route>
        <Protected exact path="/" component={Home} />
        <Protected exact path="/list" component={RestaurantList} />
        <Protected exact path="/detail" component={RestaurantDetail} />
        <Protected exact path="/search" component={RestaurantSearch} />
        <Protected exact path="/create" component={RestaurantCreate} />
        <Protected exact path="/update/:id" component={RestaurantUpdate} />
        <Route path="/logout">
          <Logout />
        </Route>
      </Router>
    </div>
  );
}

export default App;
