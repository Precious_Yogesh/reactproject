import logo from './logo.svg';
import './App.css';
import Login from './components/Login';
import RestaurantList from './components/RestaurantList';
import RestaurantDetail from './components/RestaurantDetail';
import RestaurantSearch from './components/RestaurantSearch';
import RestaurantCreate from './components/RestaurantCreate';
import RestaurantUpdate from './components/RestauranrUpdate';
import Home from './components/Home';
import { BrowserRouter as Router, Route} from 'react-router-dom'
import NavbarMenu from './components/NavbarMenu';
import Logout from './components/Logout';
import Protected from './components/Protected';


function App() {
  return (
    <div className="App">
      <Router>
        <Route path="/login" render={props => (<Login {...props} />)}>
        </Route>
        <Protected exact path="/" component={Home}/>
        <Protected path="/list" component={RestaurantList}/>
        <Protected path="/detail" component={RestaurantDetail}/>
        <Protected path="/search" component={RestaurantSearch}/>
        <Protected path="/create" component={RestaurantCreate}/>
        <Protected path="/update/:id" component={RestaurantUpdate}/>
        {/* <Route path="/update/:id" render={props => (<RestaurantUpdate{...props} />)}></Route> */}
        <Route path="/logout"><Logout/></Route>
      </Router>
    </div>
  );
}

export default App;
