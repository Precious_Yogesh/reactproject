import React, { useEffect, useState } from "react";
import NavbarMenu from "./NavbarMenu";

export default function RestaurantUpdate(props)
{
    const[name, setName]=useState('')
    const[address, setAddress]=useState('')
    const[email, setEmail]=useState('')
    const[rating, setRating]=useState('')
    const[id, setId]=useState('')

    useEffect(()=>{
        fetch('http://localhost:3000/restaurant/'+props.match.params.id).then((resp)=>{
            resp.json().then((result)=>{
                console.warn(result)
                setName(result.name)
                setAddress(result.address)
                setEmail(result.email)
                setRating(result.rating)
                setId(result.id)
            })
        })
    },[])
    function update()
    {
       let data=({name, address, email, rating})
        fetch('http://localhost:3000/restaurant/'+id,{
            method:'PUT',
            headers:{
                'Content-type':'application/json',
                'Accept':'application/json'
            },
            body:JSON.stringify(data)
        }).then((resp)=>{
            resp.json().then((result)=>{
                console.warn(result)
                alert('Restaurant Updated Successfully')
            })
        })
    }
    console.warn(props.match.params.id)
    return(
        <div>
            <NavbarMenu/>
            <h3>Restaurant Update</h3>
            <input type="text" placeholder="Restaurant Name" onChange={(e)=>{setName(e.target.value)}} value={name}/><br/><br/>
            <input type="text" placeholder="Address" onChange={(e)=>{setAddress(e.target.value)}} value={address}/><br/><br/>
            <input type="text" placeholder="Eamil" onChange={(e)=>{setEmail(e.target.value)}} value={email}/><br/><br/>
            <input type="text" placeholder="Rating" onChange={(e)=>{setRating(e.target.value)}} value={rating} /><br/><br/>
            <button onClick={()=>update()}>Update Restaurant</button>
        </div>
    )
}