import React, { useState } from "react";
import { Container, Form, Table } from 'react-bootstrap'
import {Link} from 'react-router-dom'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faEdit, faTrash} from '@fortawesome/free-solid-svg-icons' 
import NavbarMenu from "./NavbarMenu";

const RestaurantSearch = () => {
    const [data, setData] = useState('')
    const [Nodata, setNodata]=useState(false)
    const [lastsearch, setLastsearch]=useState('')

    const search = (key) => {
        setLastsearch(key)
        fetch('http://localhost:3000/restaurant?q=' + key).then((resp) => {
            resp.json().then((result) => {
                console.warn(result)
                if(result.length>0)
                {
                    setData(result)
                    setNodata(false)
                }
                else{
                    setData(null)
                    setNodata(true)
                }
                
            })
        })
    }
    function getApidata()
    {
        fetch('http://localhost:3000/restaurant').then((resp) => {
            resp.json().then((result) => {
                console.warn(result)
                setData(result)
            })
        })
    }
    function Restaurantdelete(id)
    {
        fetch('http://localhost:3000/restaurant/'+id,{
            method:'DELETE'
        }).then((resp)=>{
            resp.json().then((result)=>{
                console.warn(result)
                getApidata();
                alert('Deleted Successfully')
                search(lastsearch)
            })
        })
    }
    return (
        <Container>
            <NavbarMenu/>
            <h2>Restaurant Search</h2>
            <Form.Control type="text" placeholder="Search Restaurant" onChange={(e) => { search(e.target.value) }} />
            {
                data ?
                    <div>
                        <Table>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Restaurant Name</th>
                                    <th>Location</th>
                                    <th>Email</th>
                                    <th>Rating</th>
                                    <th>Operation</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    data.map((item) =>
                                        <tr>
                                            <td>{item.id}</td>
                                            <td>{item.name}</td>
                                            <td>{item.address}</td>
                                            <td>{item.rating}</td>
                                            <td>{item.email}</td>
                                            <td><Link to={"/update/" + item.id}><FontAwesomeIcon icon={faEdit} color="orange" /></Link>
                                                <span onClick={() => { Restaurantdelete(item.id) }}><FontAwesomeIcon icon={faTrash} color="red" /></span>
                                            </td>
                                        </tr>
                                    )
                                }
                            </tbody>
                        </Table>
                    </div>
                    :
                    null
            }
            {
                Nodata?
                <h4>No Data Found</h4>
                :
                null
            }
        </Container >
    )
}
export default RestaurantSearch;