import React, { useState } from 'react'
import NavbarMenu from './NavbarMenu'

const Login=(props)=>
{
    const[user, setUser]=useState('')
    const[password, setPassword]=useState('')

    const login=()=>
    {
        fetch('http://localhost:3000/login?q='+user).then((resp)=>{
            resp.json().then((result)=>{
                console.warn("Result", result)
                if(result.length>0)
                {
                    localStorage.setItem('login', JSON.stringify(result))
                    console.warn(props.history.push('list'))
                }
                else{
                    alert('Please Check Username and Password')
                }
            })
        })
    }
    
    return(
        <div>
            <NavbarMenu/>
            <h3>Login Page</h3>
            <input type="text" placeholder='Enter User Name' onChange={(e)=>{setUser(e.target.value)}}/><br/><br/>
            <input type="password" placeholder='Enter Password' onChange={(e)=>{setPassword(e.target.value)}}/><br/><br/>
            <button onClick={()=>login()}>Login</button>
        </div>
    )
}

export default Login;

