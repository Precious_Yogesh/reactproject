import React from 'react';
import NavbarMenu from './NavbarMenu';

const Home = () => {
    return (
        <div>
            <NavbarMenu/>
            <h2>Home</h2>
        </div>
    );
};

export default Home;