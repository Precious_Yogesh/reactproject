import React, { useEffect, useState } from 'react'
import { Table } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEdit, faTrash } from '@fortawesome/free-solid-svg-icons'
import NavbarMenu from './NavbarMenu'

const RestaurantList = () => {
    const [data, setData] = useState(null)

    function getApidata()
    {
        fetch('http://localhost:3000/restaurant').then((resp) => {
            resp.json().then((result) => {
                console.warn(result)
                setData(result)
            })
        })
    }
    useEffect(() => {
       getApidata();
    }, [])
    const listdelete=(id)=>{
        fetch('http://localhost:3000/restaurant/'+id,{
            method:'DELETE'
        }).then((resp)=>{
            resp.json().then((result)=>{
                console.warn(result)
                getApidata();
                alert('Deleted Successfully')
            })
        })
    }
    return (
        <div>
            <NavbarMenu/>
            <h4>Restaurant List</h4>
            {
                data ?
                    <div>
                        <Table>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Restaurant Name</th>
                                    <th>Address</th>
                                    <th>Email</th>
                                    <th>Rating</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    data.map((item) =>
                                        <tr>
                                            <td>{item.id}</td>
                                            <td>{item.name}</td>
                                            <td>{item.address}</td>
                                            <td>{item.email}</td>
                                            <td>{item.rating}</td>
                                            <td>
                                                <Link to={"/update/" + item.id}><FontAwesomeIcon icon={faEdit}/></Link>
                                                <span onClick={()=>listdelete(item.id)}><FontAwesomeIcon icon={faTrash} color='red' /></span>
                                            </td>
                                        </tr>
                                    )
                                }
                            </tbody>
                        </Table>
                    </div>
                    :
                    null
            }
        </div >
    )
}
export default RestaurantList;