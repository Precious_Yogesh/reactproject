import React, { useState } from "react";
import NavbarMenu from "./NavbarMenu";

const RestaurantCreate=()=>
{
    const[name, setName]=useState(null)
    const[address, setAddress]=useState(null)
    const[email, setEmail]=useState(null)
    const[rating, setRating]=useState(null)

    const CreateRestaurant=()=>{
        let data=({name, address, email, rating})
        fetch('http://localhost:3000/restaurant',{
            method:'POST',
            headers:{
                'Content-Type':'Application/json',
                'Accept':'Application/json'
            },
            body:JSON.stringify(data)
        }).then((resp)=>{
            resp.json().then((result)=>{
                console.warn(result)
                alert('Restaurant has been added')
            })
        })
    }
    return(
        <div>
            <NavbarMenu/>
            <h3>Restaurant Create</h3>
            <input type="text" placeholder="Enter Restaurant" onChange={(e)=>{setName(e.target.value)}}/><br/><br/>
            <input type="text" placeholder="Enter Address" onChange={(e)=>{setAddress(e.target.value)}}/><br/><br/>
            <input type="text" placeholder="Enter Email" onChange={(e)=>{setEmail(e.target.value)}}/><br/><br/>
            <input type="text" placeholder="Enter Rating" onChange={(e)=>{setRating(e.target.value)}}/><br/><br/>
            <button onClick={()=>CreateRestaurant()}>Add Restaurant</button>
        </div>
    )
}
export default RestaurantCreate;