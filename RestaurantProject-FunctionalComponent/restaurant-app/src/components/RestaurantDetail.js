import React from "react";
import NavbarMenu from "./NavbarMenu";

const RestaurantDetail=()=>
{
    return(
        <div>
            <NavbarMenu/>
            <h3>Restaurant Details</h3>
        </div>
    )
}
export default RestaurantDetail;